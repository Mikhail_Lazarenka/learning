package net.javaguides.hibernate;

import java.util.List;

import net.javaguides.hibernate.dao.InstructorDao;
import net.javaguides.hibernate.entity.Course;
import net.javaguides.hibernate.entity.Instructor;

//https://www.javaguides.net/2019/08/jpa-hibernate-one-to-many-unidirectional-mapping-example.html
public class ManApp {
    public static void main(String[] args) {

        InstructorDao instructorDao = new InstructorDao();

        Instructor instructor = new Instructor("Ramesh", "Fadatare", "ramesh@javaguides.com");
        instructorDao.saveInstructor(instructor);

        // create some courses
        Course tempCourse1 = new Course("Learn Spring Boot");
        final List<Course> courses1 = instructor.getCourses();
        courses1.add(tempCourse1);

        Course tempCourse2 = new Course("Learn hibernate");
        final List<Course> courses = instructor.getCourses();
        courses.add(tempCourse2);

        instructorDao.saveInstructor(instructor);
    }
}
