package io.javabrains.springsecurityjpa.models;//package io.javabrains.springsecurityjpa.models;

import lombok.Data;

import javax.persistence.*;
@Data
@Entity
@Table(name = "User")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String userName;
    private String password;
    private boolean active;
    private String roles;

}
