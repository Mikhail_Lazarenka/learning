package com.javaguides.jdbc.examples;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

//https://www.javaguides.net/2019/07/insert-and-retrieve-images-from-mysql-table-using-java.html
public class FileUploading {
    private static String cs = "jdbc:mysql://localhost:3306/jdbc-crud?useSSL=false";
    private static String user = "root";
    private static String password = "root";
    private static String sql = "INSERT INTO Images(Data) VALUES(?)";
    private static String query = "SELECT Data FROM Images LIMIT 1";

    public static void main(String[] args) {
        try (Connection con = DriverManager.getConnection(cs, user, password); PreparedStatement pst1 = con.prepareStatement(query);
                ResultSet result = pst1.executeQuery();) {
//            String path = "jdbc-crud/src/main/resources/1.jpg";
//            File file = new File(path);
//            try (FileInputStream stream = new FileInputStream(file)){
//                pst.setBinaryStream(1,stream, (int) file.length());
//                pst.executeUpdate();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
            String name = "img.jpg";

            try (FileOutputStream stream  = new FileOutputStream(name)){
                Blob blob = result.getBlob("Data");
                int len = (int)blob.length();
                byte[] buf = blob.getBytes(1, len);
                stream.write(buf, 0 , len);

            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

}
