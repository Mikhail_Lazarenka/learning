package com.example.springsecurityjpa;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @GetMapping("/")
    public String helloEveryOne(){
        return "Hello everyone";
    }
    @GetMapping("/user")
    public String helloUser(){
        return "Hello user";
    }
    @GetMapping("/admin")
    public String helloAuthenticatedUser(){
        return "Hello admin";
    }
}
