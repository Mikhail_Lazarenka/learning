package com.example.springsecurityjpa;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "user")
public class User {
    private Integer id;
    private String userName;
    private String password;
    private String roles;

}
