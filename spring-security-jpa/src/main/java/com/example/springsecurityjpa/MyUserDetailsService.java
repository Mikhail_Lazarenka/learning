package com.example.springsecurityjpa;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Optional;

@RequiredArgsConstructor
public class MyUserDetailsService implements UserDetailsService {
    private final MyUserRepository myUserRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> userByUserName = myUserRepository.findUserByUserName(username);
        userByUserName.orElseThrow(() -> new UsernameNotFoundException("Not found: " + username));
        return userByUserName.map(MyUserDetails::new).get();
    }
}
