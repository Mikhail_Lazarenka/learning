package net.javaguides.hibernate;

import net.javaguides.hibernate.dao.CourseDao;
import net.javaguides.hibernate.dao.InstructorDao;
import net.javaguides.hibernate.entity.Course;
import net.javaguides.hibernate.entity.Instructor;

//https://www.javaguides.net/2019/08/jpa-hibernate-one-to-many-bidirectional-mapping-example.html
public class ManApp {
    public static void main(String[] args) {

        InstructorDao instructorDao = new InstructorDao();
        CourseDao courseDao = new CourseDao();

        Instructor instructor = new Instructor("Ramesh", "Fadatare", "ramesh@javaguides.com");
        instructorDao.saveInstructor(instructor);

        // create some courses
        Course tempCourse1 = new Course("Air Guitar - The Ultimate Guide");
        tempCourse1.setInstructor(instructor);
        courseDao.saveCourse(tempCourse1);

        Course tempCourse2 = new Course("The Pinball Masterclass");
        tempCourse2.setInstructor(instructor);
        courseDao.saveCourse(tempCourse2);
    }
}
