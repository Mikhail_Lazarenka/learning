package b;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;
import java.util.Set;

public class B {
    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("a", "aa");
        map.put("b", "bb");
        map.put("c", "cc");
        map.put("d", "dd");
        Set<String> list = new HashSet<>();
        list.add("a");
        list.add("b");
        list.add("c");
        list.add("d");
        final Iterator<String> listIterator = list.iterator();
        while (listIterator.hasNext()) {
            if (listIterator.next() == "c") {
                listIterator.remove();
            }
        }
        System.out.println(list);

    }
}
