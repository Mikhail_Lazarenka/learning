package com.functionInterfaces;

import java.util.function.Supplier;

public class _Supplier {
    public static void main(String[] args) {
        System.out.println(getString.get());
    }

    static Supplier<String> getString = () -> "hello!";
}
