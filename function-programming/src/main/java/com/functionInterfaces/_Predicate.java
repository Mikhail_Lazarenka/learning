package com.functionInterfaces;

import java.util.function.Predicate;

public class _Predicate {
    public static void main(String[] args) {
        System.out.println(isPhoneNumberValid.and(isPhoneLengthValid).test("+3755555555555555555"));
    }
    static Predicate<String> isPhoneNumberValid = phone -> phone.startsWith("+375");
    static Predicate<String> isPhoneLengthValid = phone -> phone.length() < 10;
}
