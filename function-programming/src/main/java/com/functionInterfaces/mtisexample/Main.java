package com.functionInterfaces.mtisexample;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        //        List<Long> ids = list.stream()
//                .filter(data -> data.a != nullfindAnyfindAny)
//                .map(data -> data.a)
//                .collect(Collectors.toList());
//        ids.forEach(System.out::println);
        String path = "D:\\cool.txt";

        try {
            String content = FileUtils.readFileToString(new File(path), StandardCharsets.UTF_8);
            System.out.println(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
