package com.functionInterfaces.mtisexample;

public class A {
    Long a;
    B b;
    Long c;

    public A(Long a, B b, Long c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Long getC() {
        return c;
    }

    public void setC(Long c) {
        this.c = c;
    }

    public Long getA() {
        return a;
    }

    public void setA(Long a) {
        this.a = a;
    }

    public B getB() {
        return b;
    }

    public void setB(B b) {
        this.b = b;
    }

    @Override
    public String toString() {
        return "A{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
