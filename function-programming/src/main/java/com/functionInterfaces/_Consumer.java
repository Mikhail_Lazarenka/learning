package com.functionInterfaces;

import java.util.function.Consumer;

public class _Consumer {
    public static void main(String[] args) {
        sayHelloCustomer.accept("Mike");
    }

    static Consumer<String> sayHelloCustomer =  System.out::println;
}
