package com.functionInterfaces;

import java.util.function.BiFunction;
import java.util.function.Function;

public class _Function {
    public static void main(String[] args) {
        System.out.println(incrementByOneFunction.andThen(multiplyBy10Function).apply(1));
        System.out.println(incrementByOneFunction.compose(multiplyBy10Function).apply(1));
        System.out.println(incrementAndMultiply.apply(1,1));
    }
    static Function<Integer, Integer> incrementByOneFunction =
            number -> number + 1;

    static Function<Integer, Integer> multiplyBy10Function =
            number -> number * 10;

    static BiFunction<Integer, Integer, Integer> incrementAndMultiply =
            (number1, number2) -> ((number1 + number2) * 10);
}
