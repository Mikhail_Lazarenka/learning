package com.optional;

import java.util.Optional;

public class _Optional {
    public static void main(String[] args) {
        Optional<String> hello = Optional.ofNullable("Mike");
        hello.ifPresent(System.out::println);
        hello.ifPresentOrElse(System.out::println, () -> System.out.println("Error"));
    }
}
