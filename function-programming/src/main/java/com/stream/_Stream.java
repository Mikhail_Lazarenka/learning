package com.stream;

import java.util.List;
import java.util.stream.Collectors;

public class _Stream {
    public static void main(String[] args) {
        List<Person> people = List.of(
                new Person("Mike", Gender.MALE, "1"),
                new Person("Mike", Gender.MALE, "2"),
                new Person("Mike", Gender.MALE, "1"),
                new Person("Mike", Gender.MALE, "1"),
                new Person("Mike", Gender.MALE, null),
                new Person("Mike", Gender.MALE, null),

                new Person("Ana", Gender.FEMALE, "2"),
                new Person("Ana", Gender.FEMALE, "2"),
                new Person("Ana", Gender.FEMALE, null)
        );
        people.stream()
                .map(person -> person.getGender())
                .collect(Collectors.toList())
                .forEach(System.out::println);

        people.stream()
                .noneMatch(person -> Gender.FEMALE.equals(person.getGender()));
//        people.stream()
//                .map(person -> person.getIp())
//                .collect(Collectors.toList())
//                .
    }

    static class Person{
        private final String name;
        private final Gender gender;
        private final String ip;


        Person(String name, Gender gender, String ip) {
            this.name = name;
            this.gender = gender;
            this.ip = ip;
        }

        public String getName() {
            return name;
        }

        public Gender getGender() {
            return gender;
        }

        public String getIp() {
            return ip;
        }
    }

    enum Gender{
        MALE,
        FEMALE
    }
}
