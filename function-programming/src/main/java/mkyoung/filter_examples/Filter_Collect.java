package mkyoung.filter_examples;

import java.util.List;
import java.util.stream.Collectors;

public class Filter_Collect {
    public static void main(String[] args) {

        List<String> list = List.of("mike", "anna", "antony");
        List<String> newList = list.stream()
                .filter(p -> !p.equalsIgnoreCase("anna"))
                .collect(Collectors.toList());
        newList.forEach(System.out::println);
    }
}
