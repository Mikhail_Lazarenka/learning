package mkyoung.filter_examples;

import java.util.List;
import java.util.stream.Collectors;

public class Filter_Map {
    public static void main(String[] args) {
        List<Person> people = List.of(new Person("mike", 12),
                new Person("anna", 42),
                new Person("mike", 52),
                new Person("a", 13));
        String name = people.stream()
                .filter(p -> "mike".equalsIgnoreCase(p.getName()))
                .map(Person::getName)
                .findAny()
                .orElse("");
        System.out.println(name);
        List<String> names = people.stream()
                .map(Person::getName)
                .collect(Collectors.toList());
        System.out.println(names);
    }
}
