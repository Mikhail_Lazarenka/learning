package mkyoung.filter_examples;

import java.util.List;

public class Filter_FindAny_orElse {
    public static void main(String[] args) {
        List<Person> people = List.of(new Person("mike", 12),
                new Person("anna", 42),
                new Person("sdfsdf", 52),
                new Person("a", 13));
        Person anna = people.stream()
                .filter(p -> p.getName().equalsIgnoreCase("anna"))
                .findAny()
                .get();
        System.out.println(anna);

        Person a = people.stream()
                .filter(p -> p.getName().equalsIgnoreCase("anna"))
                .findAny()
                .orElse(null);
        System.out.println(a);
    }
}
