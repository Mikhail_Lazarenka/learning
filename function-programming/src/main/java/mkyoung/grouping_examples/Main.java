package mkyoung.grouping_examples;

import mkyoung.Worker;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<Worker> workers = List.of(
                new Worker("Mike", 23, 2000, "engineer"),
                new Worker("Mike2", 26, 2000, "engineer"),
                new Worker("Alex", 22, 200, "builder"),
                new Worker("Anna", 26, 100, "architecture"),
                new Worker("Antony", 10, 1356, "solder"),
                new Worker("An", 10, 1356, "solder")
                );
        Map<String, List<Worker>> position = workers.stream()
                .collect(Collectors.groupingBy(Worker::getPosition));
        System.out.println(position);

//        workers.stream()
//                .collect(Collectors.groupingBy(Worker::getPosition, Collectors.mapping(Worker::getName, Collectors.toList() ));

    }
}
