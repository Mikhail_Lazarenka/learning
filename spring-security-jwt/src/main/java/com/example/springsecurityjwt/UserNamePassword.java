package com.example.springsecurityjwt;

import lombok.Data;

@Data
public class UserNamePassword {
    private String userName;
    private String password;
}
