package com.newblockchain;

import com.blockchain.StringUtil;

public class Block {
    String hash;
    String previousHash;
    String data;
    Long nonce;

    public Block(String data, String previousHash) {
        this.data = data;
        this.previousHash = previousHash;

    }
    public String calculateHash() {
        String calculatedHash = StringUtil.applySha256(
                previousHash +
                        Long.toString(nonce) +
                        data
        );
        return calculatedHash;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }

    public void setPreviousHash(String previousHash) {
        this.previousHash = previousHash;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public Long getNonce() {
        return nonce;
    }

    public void setNonce(Long nonce) {
        this.nonce = nonce;
    }
}
